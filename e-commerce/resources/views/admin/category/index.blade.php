@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card header">
            <h4>Category Page</h4>
            <hr>
        </div>
        <div class="card-body">
            <div class="table table-border table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($category as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->description }}</td>
                        <td>
                            <img src="{{ asset('assets/uploads/category/'.$item->image) }}" class="w-25" alt="Image here">
                        </td>
                        <td>
                            <button herf="{{ url('edit-prod/'.$item->id)}}" class="btn btn-primary">Edit</button>
                            <button herf="{{ url('delete-category/'.$item->id)}}" class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </div>
        </div>
    </div>
@endsection